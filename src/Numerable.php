<?php

namespace Genby\Numerable;

class Numerable
{
	/**
	 * Возвращает слово с окончанием подходящим к цифре.
	 *
	 * @param int $number <p>Цифра</p>
	 * @param string $for1 <p>Подходящее слово для одного количества</p>
	 * @param string $for2to4 <p>Подходящее слово для количества от 2 до 4</p>
	 * @param string $forMore <p>Подходящее слово для большего количества</p>
	 * @return string <p>Подходящее слово</p>
	 */
	public function get($number, $for1, $for2to4, $forMore)
	{
		$pos = strlen($number);
		$units = substr($number, $pos - 1, 1);
		$tens = 0;
		if (strlen($number) > 1) {
			$tens = substr($number, $pos - 2, 1);
		}
		switch ($units) {
			case 1 :
				if ($tens !== false && $tens == 1) return $forMore;
				return $for1;
			case 2 :
				if ($tens !== false && $tens == 1) return $forMore;
				return $for2to4;
			case 3 :
				if ($tens !== false && $tens == 1) return $forMore;
				return $for2to4;
			case 4 :
				if ($tens !== false && $tens == 1) return $forMore;
				return $for2to4;
			default :
				return $forMore;
		}
	}
}