`print (new \Genby\Numerable\Numerable())->get(1, 'гвоздь', 'гвоздя', 'гвоздей');`

выведет: гвоздь

`print (new \Genby\Numerable\Numerable())->get(2, 'гвоздь', 'гвоздя', 'гвоздей');`

выведет: гвоздя

`print (new \Genby\Numerable\Numerable())->get(5, 'гвоздь', 'гвоздя', 'гвоздей');`

выведет: гвоздей
